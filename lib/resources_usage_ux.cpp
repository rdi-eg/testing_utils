#include "rdi_resources_usage_ux.hpp"
#include <rdi_stl_utils.hpp>

#include <string.h>
#include <iostream>
#include <sstream>
#include <memory>

#include <sys/types.h>
#include <sys/resource.h>
#include <unistd.h>
#include <errno.h>

using namespace std;

namespace RDI
{

inline string read_line(ifstream& fh)
{
    string line;
    int pos = fh.tellg();
    getline(fh, line);
    fh.seekg(pos);
    return line;
}

RDI::Result<long unsigned int> read_cpu_total_time(ifstream& fh)
{
    // read execution time processes in different modes (user, nice, system, idle, ...)
    vector<string> values = RDI::split(read_line(fh));

    long unsigned int cpu_total_time = 0;
    // ignore the first item 'CPU'
    for(size_t i = 1; i < values.size(); i++)
    {
        cpu_total_time += std::stoul(values[i]);
    }

    return {cpu_total_time, RDI::Error{}};
}

RDI::Result<long unsigned int> read_cpu_total_time()
{
    ifstream fh("/proc/stat");
    return read_cpu_total_time(fh);
}

RDI::Result<float> get_proc_ram_usage()
{
    static const float KB = 1024.0f;
    auto output = read_proc_resources_usage();
    if(output.error.code != 0)
    {
        return {-1.0f, output.error};
    }

    float wss_kb =  output.result.wss / KB;
    return {wss_kb, RDI::Error{}};
}

RDI::Result<ProcStat> read_proc_resources_usage(ifstream& fh)
{
    vector<string> values = RDI::split(read_line(fh));

    // pick only the fields we are interested in.
    ProcStat proc_stat = {};
    proc_stat.utime_ticks  = std::stoul(values[13]);
    proc_stat.stime_ticks  = std::stoul(values[14]);
    proc_stat.cutime_ticks = std::stoul(values[15]);
    proc_stat.cstime_ticks = std::stoul(values[16]);
    proc_stat.vsize        = std::stoul(values[22]);
    proc_stat.wss          =  std::stoul(values[23]) * getpagesize(); // resident set size (physical memory)
    
    return {proc_stat, RDI::Error{}};
}

RDI::Result<ProcStat> read_proc_resources_usage()
{
    ifstream fh ("/proc/" + to_string(getpid()) + "/stat");
    return read_proc_resources_usage(fh);
}

float 
compute_proc_cpu_usage(LUI last_cpu_time, LUI cur_cpu_time,
                   LUI last_proc_time, LUI cur_proc_time)
{
    long unsigned int cpu_diff_time = cur_cpu_time - last_cpu_time;
    long unsigned int proc_diff_time = cur_proc_time - last_proc_time;
    return (proc_diff_time * 1.0f / cpu_diff_time) * 100.0f; 
}

} // namespace RDI