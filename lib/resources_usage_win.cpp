#include "rdi_resources_usage_win.hpp"

#include <windows.h>
#include <psapi.h>

namespace RDI
{

inline RDI::Error reading_proc_info_failure()
{
    return  RDI::Error{READING_PROCESSINFO_FAILURE,
                        "can't read the process Information"};
}

static int cpus_number()
{
    SYSTEM_INFO sysInfo;
    GetSystemInfo(&sysInfo);
    return sysInfo.dwNumberOfProcessors;
}

RDI::Result<ULONGLONG> read_cpu_total_time()
{
    FILETIME ftime;
    GetSystemTimeAsFileTime(&ftime);

    _ULARGE_INTEGER ftime_x;
    memcpy(&ftime_x, &ftime, sizeof(FILETIME));

    return RDI::Result<ULONGLONG>{ftime_x.QuadPart, RDI::Error{}};
}

RDI::Result<size_t> get_proc_ram_usage()
{
    PROCESS_MEMORY_COUNTERS ps_info;
    BOOL status = GetProcessMemoryInfo( GetCurrentProcess( ), 
                                        &ps_info, 
                                        sizeof(ps_info) );
    if(status == false)
    {
        return RDI::Result<size_t>{0, reading_proc_info_failure()};
    }

    size_t wss = (size_t) ps_info.WorkingSetSize; // resident set size (ram usage now).
    return RDI::Result<size_t>{wss, RDI::Error{}};
}

RDI::Result<ProcStat> read_proc_resources_usage()
{
    // read the user mode execution time and kernel mode execution time.
    FILETIME creation_time, exit_time, kernel_time, usr_time;
    HANDLE ps_handle = GetCurrentProcess();
    BOOL status = GetProcessTimes(ps_handle, &creation_time, &exit_time, 
                                 &kernel_time, &usr_time);
    if(status == false)
    {
        return RDI::Result<ProcStat>{ProcStat(), reading_proc_info_failure()};
    }

    ULARGE_INTEGER kernel_time_x, usr_time_x;
    memcpy(&kernel_time_x, &kernel_time, sizeof(FILETIME));
    memcpy(&usr_time_x, &usr_time, sizeof(FILETIME));

    ProcStat proc_stat;
    proc_stat.kernel_time = kernel_time_x.QuadPart;
    proc_stat.user_time = usr_time_x.QuadPart;

    // read the working set size.
    proc_stat.wss = get_proc_ram_usage().result;

    return RDI::Result<ProcStat>{proc_stat, RDI::Error{}};
}

float compute_proc_cpu_usage(ULONGLONG last_cpu_time,
                            ULONGLONG cur_cpu_time,
                            ULONGLONG last_proc_cpu_time,
                            ULONGLONG cur_proc_cpu_time)
{
    ULONGLONG proc_cpu_time_diff = (cur_proc_cpu_time - last_proc_cpu_time);
    ULONGLONG cpu_time_diff = cur_cpu_time - last_cpu_time;
    float percentage = (proc_cpu_time_diff * 1.0f / cpu_time_diff) * 100.0f;
    
    return isnan(percentage)? 0.0f : percentage;
}
}