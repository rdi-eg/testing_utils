#ifndef RDI_RESOURCES_USAGE_H
#define RDI_RESOURCES_USAGE_H

#include <iostream>
#include <fstream>
#ifdef _WIN32
#else 
#include <stdio.h>
#endif

#include <rdi_result.hpp>

using LUI = long unsigned int;

namespace RDI
{
// process stat in a specific timestamp (time is measured in jiffy unit)
struct ProcStat {
    long unsigned int utime_ticks;   // process user mode execution time. 
    long unsigned int cutime_ticks;  // process childs user mode execution time.
    long unsigned int stime_ticks;   // process kernel mode exection time.
    long unsigned int cstime_ticks;  // process's childs kernel mode execution time.
    long unsigned int vsize;         // virtual memory size in KB (even the memory that swapped out)
    long unsigned int wss;           //Resident  Set  Size in KB (only used by the RAM)

    // process usr mode time + kernel mode time.
    long unsigned int main_process_time() const
    {
        return utime_ticks + stime_ticks;
    }

    // process childs usr mode time + kernel mode time.
    long unsigned int childs_processes_time() const
    {
        return cutime_ticks + cstime_ticks;
    }

    // process execution time (itself + its childs)
    long unsigned int total_execution_time () const
    {
        return main_process_time() + childs_processes_time();
    }

    // display the process stat
    void display() const
    {
        std::cout << "utime_ticks: " << utime_ticks << std::endl;
        std::cout << "stime_ticks: " << stime_ticks << std::endl;
        std::cout << "cutime_ticks: " << cutime_ticks << std::endl;
        std::cout << "cstime_ticks: " << cstime_ticks << std::endl;
        std::cout << "vsize: "        << vsize << std::endl;
        std::cout << "wss: "          << wss << std::endl;
    }
}; 

/** @brief read the cpu total time at a specific timestamp.
 * various pieces of information about kernel/system activity are available in the /proc/stat file 
 * you can read more about the statistics hold by this file here 
 * https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk65143
 * @param fh: the handler of /proc/stat file 
 * @return the cpu_total time (ticks) since the system was boot.
*/
RDI::Result<long unsigned int> read_cpu_total_time(std::ifstream& fh);

// the same as read_cpu_total_time(const FILE* ) but it opens the file for you.
RDI::Result<long unsigned int> read_cpu_total_time();

// return the used memory used by the process in KB
RDI::Result<float> get_proc_ram_usage();

/** @brief read the process resources usage (pstat info) at a specific timestamp.
 * various pieces of information about the process is logged in /proc/<PID>/stat
 * you can read more about the statistics hold by this file about the running process
 * https://stackoverflow.com/a/60441542 
 * @param fh: the handler of /proc/<PID>/stat file 
 * @return ProcStat: information about the kernel/user time used by the process and memrory.
*/
RDI::Result<ProcStat> read_proc_resources_usage(std::ifstream& fh);

// the same as read_proc_resources_usage(const FILE* ) but it opens the file for you.
RDI::Result<ProcStat> read_proc_resources_usage();

// proc_cpu_usage = ((cur_proc_time - last_proc_time)/(cur_cpu_time - last_cpu_time)) * 100.0f
float compute_proc_cpu_usage(LUI last_cpu_time, LUI cur_cpu_time,
                         LUI last_proc_time, LUI cur_proc_time);
}

#endif