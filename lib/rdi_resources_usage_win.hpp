#ifndef RDI_RESOURCES_USAGE_WIN_HPP
#define RDI_RESOURCES_USAGE_WIN_HPP

#include <rdi_result.hpp>
#include <windows.h>

namespace RDI {

#define READING_PROCESSINFO_FAILURE 1547

struct ProcStat {
    ULONGLONG user_time;    // process user mode execution time.
    ULONGLONG kernel_time;  // process kernel mode exection time.
    size_t wss;						//Working  Set  Size in KB (only used by the RAM)

    // process usr mode time + kernel mode time.
    ULONGLONG total_execution_time() const
    {
        return user_time + kernel_time;
    }
};
	
// return the elapsed time since the system boot up.
RDI::Result<ULONGLONG> read_cpu_total_time();

// get information about the process resources usage like ram, kernel time, user tim
RDI::Result<ProcStat> read_proc_resources_usage();

// return the used memory used by the process in Bytes
RDI::Result<size_t> get_proc_ram_usage();

// compute the percentage usage from the last time till now
float compute_proc_cpu_usage(ULONGLONG last_cpu_time,
                            ULONGLONG cur_cpu_time,
                            ULONGLONG last_proc_time,
                            ULONGLONG cur_proc_time);
} // namespace RDI
#endif