cmake_minimum_required(VERSION 3.16)
project(rdi_testing_utils)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

# clone cmake_scripts
set(cmake_scripts_path "${CMAKE_BINARY_DIR}/cmake_scripts")

if(NOT EXISTS "${cmake_scripts_path}")
	execute_process(
		COMMAND
			git clone "git@arwash.rdi:common/cmake_scripts.git" ${cmake_scripts_path}
		RESULT_VARIABLE
			return_status
	)
else()
	execute_process(
		COMMAND
			git pull
		WORKING_DIRECTORY
			${cmake_scripts_path}
		RESULT_VARIABLE
			return_status
	)
endif()

if(NOT ${return_status} EQUAL 0)
	message(FATAL_ERROR "git clone/pull failed")
endif()

if(NOT workspace_location)
	set(workspace_location ${CMAKE_CURRENT_SOURCE_DIR}/../..)
endif()

# convert workspace_location to an absolute path
get_filename_component(workspace_location ${workspace_location} ABSOLUTE)

if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Debug)
endif()

include(${cmake_scripts_path}/rdi_build_path.cmake)
rdi_build_path(${CMAKE_SOURCE_DIR} build_path)

add_subdirectory(lib ${build_path}/lib)
